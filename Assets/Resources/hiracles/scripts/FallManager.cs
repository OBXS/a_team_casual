﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallManager : MonoBehaviour
{
    enum SpriteType
    {
        BG_0,

        BG_1,

        BG_2,

        BG_3,

        BG_4,

        BG_MASK,

        CHARA,

        OBSTACLE_LEFT,

        OBSTACLE_RIGHT,

        WALL_LEFT,

        WALL_RIGHT,

        SPRITE_TYPE_COUNT,
    }

    public bool is_chara_;

    public bool is_obstacle_;

    public bool is_wall_;

    private GameObject prefab_;

    private List<GameObject> sprites_;

    private List<SpriteRenderer> renderers_;

    private float scroll_speed_;

    private void Awake()
    {
        prefab_ = Resources.Load<GameObject>("hiracles/prefab");

        sprites_ = new List<GameObject>();

        renderers_ = new List<SpriteRenderer>();

        scroll_speed_ = 0.01f;
    }

    private void Start()
    {
        {
            sprites_.Add(CreateSprite(
                new Vector3(0, -25f, 20f),
                "fall_bg_0",
                new Vector3(0.75f, 0.75f, 1f)));

            sprites_.Add(CreateSprite(
                new Vector3(0f, -21.25f, 30f),
                "fall_bg_1",
                new Vector3(1f, 1f, 1f)));

            sprites_.Add(CreateSprite(
                new Vector3(0f, -17.5f, 40f),
                "fall_bg_2",
                new Vector3(1.25f, 1.25f, 1f)));

            sprites_.Add(CreateSprite(
                new Vector3(0f, -13.75f, 50f),
                "fall_bg_3",
                new Vector3(1.5f, 1.5f, 1f)));

            sprites_.Add(CreateSprite(
                new Vector3(0f, -10f, 60f),
                "fall_bg_4",
                new Vector3(1.75f, 1.75f, 1f)));

            for (int i = 0; i < (int)SpriteType.BG_MASK; ++i)
            {
                renderers_.Add(sprites_[i].GetComponent<SpriteRenderer>());

                renderers_[i].sprite = Resources.Load<Sprite>(
                    "hiracles/textures/fall_bg_" + i);
            }
        }

        {
            sprites_.Add(CreateSprite(
                new Vector3(0f, 0f, 10f),
                "fall_bg_mask",
                new Vector3(0.5f, 0.5f, 1f)));

            renderers_.Add(sprites_[(int)SpriteType.BG_MASK].GetComponent<SpriteRenderer>());

            renderers_[(int)SpriteType.BG_MASK].sprite = Resources.Load<Sprite>(
                "hiracles/textures/fall_bg_mask");

            renderers_[(int)SpriteType.BG_MASK].color = new Color(1f, 1f, 1f, 0.25f);
        }


        if (is_chara_)
        {
            //ここに使いました
            sprites_.Add(CreateSprite(
                    Resources.Load<GameObject>("Player/PlayerComplete"),
                    new Vector3(0f, 0f, 0f),
                    "player",
                    new Vector3(0.085f, 0.085f, 1f)));

            renderers_.Add(sprites_[(int)SpriteType.CHARA].GetComponent<SpriteRenderer>());

            renderers_[(int)SpriteType.CHARA].sprite = Resources.Load<Sprite>(
                "hiracles/textures/title_chara");

            sprites_[(int)SpriteType.CHARA].SetActive(is_chara_);
        }

        if (is_obstacle_)
        {
            sprites_.Add(CreateSprite(
                new Vector3(-2.5f, -10f, 0f),
                "obstacle_left",
                new Vector3(0.5f, 0.5f, 1f)));

            renderers_.Add(sprites_[(int)SpriteType.OBSTACLE_LEFT].GetComponent<SpriteRenderer>());

            renderers_[(int)SpriteType.OBSTACLE_LEFT].sprite = Resources.Load<Sprite>(
                "hiracles/textures/player_color");

            ObstacleController controller =
                sprites_[(int)SpriteType.OBSTACLE_LEFT].AddComponent<ObstacleController>();

            controller.scroll_limit_ = 10f;
            controller.warp_position_ = -10f;

            sprites_[(int)SpriteType.OBSTACLE_LEFT].AddComponent<BoxCollider>();

            sprites_.Add(CreateSprite(
                new Vector3(2.5f, 0f, 0f),
                "obstacle_right",
                new Vector3(0.5f, 0.5f, 1f)));

            renderers_.Add(sprites_[(int)SpriteType.OBSTACLE_RIGHT].GetComponent<SpriteRenderer>());

            renderers_[(int)SpriteType.OBSTACLE_RIGHT].sprite = Resources.Load<Sprite>(
                "hiracles/textures/player_color");

            controller = sprites_[(int)SpriteType.OBSTACLE_RIGHT].AddComponent<ObstacleController>();

            controller.scroll_limit_ = 10f;
            controller.warp_position_ = -10f;

            sprites_[(int)SpriteType.OBSTACLE_RIGHT].AddComponent<BoxCollider>();
        }

        if (is_wall_)
        {
            sprites_.Add(CreateSprite(
                new Vector3(-3.5f, 0f, 0f),
                "wall_left",
                new Vector3(0.5f, 5f, 1f)));
            
            renderers_.Add(sprites_[(int)SpriteType.WALL_LEFT].GetComponent<SpriteRenderer>());

            renderers_[(int)SpriteType.WALL_LEFT].sprite = Resources.Load<Sprite>(
                "hiracles/textures/player_color");

            sprites_.Add(CreateSprite(
                new Vector3(3.5f, 0f, 0f),
                "wall_right",
                new Vector3(0.5f, 5f, 1f)));

            renderers_.Add(sprites_[(int)SpriteType.WALL_RIGHT].GetComponent<SpriteRenderer>());

            renderers_[(int)SpriteType.WALL_RIGHT].sprite = Resources.Load<Sprite>(
                "hiracles/textures/player_color");
        }
    }

    private void Update()
    {
        for (int i = 0; i < (int)SpriteType.BG_MASK; ++i)
        {
            sprites_[i].transform.position = new Vector3(
                sprites_[i].transform.position.x,
                sprites_[i].transform.position.y + scroll_speed_,
                sprites_[i].transform.position.z);
        }
    }

    private GameObject CreateSprite(string name, Vector3 scale)
    {
        GameObject sprite = Instantiate(prefab_);

        sprite.name = name;

        sprite.transform.localScale = scale;

        return sprite;
    }

    private GameObject CreateSprite(Vector3 position, string name, Vector3 scale)
    {
        GameObject sprite = Instantiate(prefab_, position, Quaternion.identity);

        sprite.name = name;

        sprite.transform.localScale = scale;

        return sprite;
    }

    //プレイヤーの特別のprefabに使います
    private GameObject CreateSprite(GameObject gameObject, Vector3 position, string name, Vector3 scale)
    {
        GameObject sprite = Instantiate(gameObject, position, Quaternion.identity);

        sprite.name = name;

        sprite.transform.localScale = scale;

        return sprite;
    }
}
