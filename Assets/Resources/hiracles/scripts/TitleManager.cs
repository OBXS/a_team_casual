﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TitleManager : MonoBehaviour
{
    enum SpriteType
    {
        TITLE_BG,

        TITLE_CHARA,

        TITLE_ABYSS,

        TITLE_START,

        PAGE_0,

        PAGE_1,

        PAGE_2,

        PAGE_3,

        PAGE_4,

        PAGE_5,

        SPRITE_TYPE_COUNT,
    }

    private GameObject prefab_;

    private List<GameObject> sprites_;

    private List<SpriteRenderer> renderers_;

    public int time_a_;

    private float flip_;

    private int current_page_;

    private float fade_speed_;

    private bool is_fade_;

    private void Awake()
    {
        prefab_ = Resources.Load<GameObject>("hiracles/prefab");

        sprites_ = new List<GameObject>();

        renderers_ = new List<SpriteRenderer>();

        time_a_ = 0;

        flip_ = 1f;

        current_page_ = (int)SpriteType.PAGE_0;

        fade_speed_ = 0.01f;

        is_fade_ = true;
    }

    private void Start()
    {
        // タイトルの背景
        {
            sprites_.Add(CreateSprite(
                new Vector3(0f, 0f, 0.1f),
                "title_bg",
                new Vector3(0.286f, 0.286f, 1f)));

            renderers_.Add(sprites_[(int)SpriteType.TITLE_BG].GetComponent<SpriteRenderer>());

            renderers_[(int)SpriteType.TITLE_BG].sprite =
                Resources.Load<Sprite>("hiracles/textures/title_bg");

            renderers_[(int)SpriteType.TITLE_BG].color = new Color(
                renderers_[(int)SpriteType.TITLE_BG].color.r,
                renderers_[(int)SpriteType.TITLE_BG].color.g,
                renderers_[(int)SpriteType.TITLE_BG].color.b,
                0f);
        }

        // タイトルのキャラクター
        {
            sprites_.Add(CreateSprite(
                new Vector3(-0.3f, 3.1f, 0f),
                Quaternion.Euler(0f, 0f, -45f),
                "title_chara",
                new Vector3(0.1f, 0.1f, 1f)));

            sprites_[(int)SpriteType.TITLE_CHARA].SetActive(false);

            renderers_.Add(sprites_[(int)SpriteType.TITLE_CHARA].GetComponent<SpriteRenderer>());

            renderers_[(int)SpriteType.TITLE_CHARA].sprite =
                Resources.Load<Sprite>("hiracles/textures/title_chara");
        }

        // タイトルの "Abyss" ロゴ
        {
            sprites_.Add(CreateSprite(
                new Vector3(0.075f, -0.75f, 0f),
                "title_abyss",
                new Vector3(0.15f, 0.15f, 1f)));

            renderers_.Add(sprites_[(int)SpriteType.TITLE_ABYSS].GetComponent<SpriteRenderer>());

            renderers_[(int)SpriteType.TITLE_ABYSS].sprite =
                Resources.Load<Sprite>("hiracles/textures/title_abyss");

            renderers_[(int)SpriteType.TITLE_ABYSS].color = new Color(
                renderers_[(int)SpriteType.TITLE_ABYSS].color.r,
                renderers_[(int)SpriteType.TITLE_ABYSS].color.g,
                renderers_[(int)SpriteType.TITLE_ABYSS].color.b,
                0f);
        }

        // タイトルの "Start" ロゴ
        {
            sprites_.Add(CreateSprite(
                new Vector3(0.05f, -2.5f, 0f),
                "title_start",
                new Vector3(0.25f, 0.25f, 1f)));

            renderers_.Add(sprites_[(int)SpriteType.TITLE_START].GetComponent<SpriteRenderer>());

            renderers_[(int)SpriteType.TITLE_START].sprite =
                Resources.Load<Sprite>("hiracles/textures/title_start");

            renderers_[(int)SpriteType.TITLE_START].color = new Color(
                renderers_[(int)SpriteType.TITLE_START].color.r,
                renderers_[(int)SpriteType.TITLE_START].color.g,
                renderers_[(int)SpriteType.TITLE_START].color.b,
                0f);
        }

        // ストーリーページ
        {
            for (int i = 0; i < 6; ++i)
            {
                sprites_.Add(CreateSprite(
                    new Vector3(0f, 0f, i * 0.1f),
                    "page_" + i,
                    new Vector3(0.188f, 0.188f, 1f)));

                renderers_.Add(sprites_[(int)SpriteType.PAGE_0 + i].GetComponent<SpriteRenderer>());

                renderers_[(int)SpriteType.PAGE_0 + i].sprite =
                    Resources.Load<Sprite>("hiracles/textures/page_" + i);

                if (i != 0 && i != 5)
                {
                    renderers_[(int)SpriteType.PAGE_0 + i].material =
                        Resources.Load<Material>("hiracles/materials/next_page");
                }

                if (i == 0)
                {
                    renderers_[(int)SpriteType.PAGE_0].color = new Color(
                        renderers_[(int)SpriteType.PAGE_0].color.r,
                        renderers_[(int)SpriteType.PAGE_0].color.g,
                        renderers_[(int)SpriteType.PAGE_0].color.b,
                        0f);
                }
                else
                {
                    sprites_[(int)SpriteType.PAGE_0 + i].SetActive(false);
                }
            }
        }
    }

    private void Update()
    {
        if (renderers_[(int)SpriteType.PAGE_0].color.a >= 1f)
        {
            if (time_a_ == 120)
            {
                if (current_page_ == (int)SpriteType.PAGE_5)
                {
                    if (renderers_[(int)SpriteType.PAGE_5].color.a <= 0f)
                    {
                        if (!sprites_[(int)SpriteType.TITLE_CHARA].activeInHierarchy)
                        {
                            sprites_[(int)SpriteType.TITLE_CHARA].SetActive(true);
                        }

                        if (renderers_[(int)SpriteType.TITLE_BG].color.a >= 1f)
                        {
                            if (renderers_[(int)SpriteType.TITLE_ABYSS].color.a >= 1f)
                            {
                                if (is_fade_)
                                {
                                    if (renderers_[(int)SpriteType.TITLE_START].color.a >= 1f)
                                    {
                                        is_fade_ = false;
                                    }
                                    else
                                    {
                                        FadeInSprite(renderers_[(int)SpriteType.TITLE_START]);
                                    }
                                }
                                else
                                {
                                    if (renderers_[(int)SpriteType.TITLE_START].color.a <= 0f)
                                    {
                                        is_fade_ = true;
                                    }
                                    else
                                    {
                                        FadeOutSprite(renderers_[(int)SpriteType.TITLE_START]);
                                    }
                                }
                            }
                            else
                            {
                                sprites_[(int)SpriteType.TITLE_ABYSS].transform.position = new Vector3(
                                    sprites_[(int)SpriteType.TITLE_ABYSS].transform.position.x,
                                    sprites_[(int)SpriteType.TITLE_ABYSS].transform.position.y + fade_speed_,
                                    sprites_[(int)SpriteType.TITLE_ABYSS].transform.position.z);

                                FadeInSprite(renderers_[(int)SpriteType.TITLE_ABYSS]);
                            }
                        }
                        else
                        {
                            FadeInSprite(renderers_[(int)SpriteType.TITLE_BG]);
                        }
                    }
                    else
                    {
                        FadeOutSprite(renderers_[(int)SpriteType.PAGE_5]);
                    }
                }
                else
                {
                    if (flip_ > -1)
                    {
                        flip_ -= 0.02f;

                        renderers_[current_page_].material.SetFloat("_Flip", flip_);
                    }
                    else
                    {
                        time_a_ = 0;

                        flip_ = 1f;

                        ++current_page_;
                    }
                }
            }
            else
            {
                ++time_a_;
            }
        }
        else
        {
            FadeInSprite(renderers_[current_page_]);

            if(renderers_[current_page_].color.a >= 1f)
            {
                renderers_[current_page_].material =
                    Resources.Load<Material>("hiracles/materials/next_page");

                for (int i = (int)SpriteType.PAGE_0; i < (int)SpriteType.SPRITE_TYPE_COUNT; ++i)
                {
                    sprites_[i].SetActive(true);
                }
            }
        }
    }

    private GameObject CreateSprite(string name, Vector3 scale)
    {
        GameObject sprite = Instantiate(prefab_);

        sprite.name = name;

        sprite.transform.localScale = scale;

        return sprite;
    }

    private GameObject CreateSprite(Vector3 position, string name, Vector3 scale)
    {
        GameObject sprite = Instantiate(prefab_, position, Quaternion.identity);

        sprite.name = name;

        sprite.transform.localScale = scale;

        return sprite;
    }

    private GameObject CreateSprite(
        Vector3    position,
        Quaternion rotation,
        string     name,
        Vector3    scale)
    {
        GameObject sprite = Instantiate(prefab_, position, rotation);

        sprite.name = name;

        sprite.transform.localScale = scale;

        return sprite;
    }

    private void FadeInSprite(SpriteRenderer renderer)
    {
        renderer.color = new Color(
            renderer.color.r,
            renderer.color.g,
            renderer.color.b,
            renderer.color.a + fade_speed_);
    }

    private void FadeOutSprite(SpriteRenderer renderer)
    {
        renderer.color = new Color(
            renderer.color.r,
            renderer.color.g,
            renderer.color.b,
            renderer.color.a - fade_speed_);
    }
}
