﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleController : MonoBehaviour
{
    enum Child
    {
        LEFT,

        TOP,

        RIGHT,

        BOTTOM,

        TOP_LEFT,

        TOP_RIGHT,

        BOTTOM_RIGHT,

        BOTTOM_LEFT,

        CHILD_COUNT,
    }

    private float scroll_speed_;

    public bool is_pause_;

    public float scroll_limit_;

    public float warp_position_;

    /// <summary>
    /// 初期化処理をします
    /// </summary>
    private void Start()
    {
        InitializeScale();

        is_pause_ = false;

        scroll_speed_  = 0.1f;
        // scroll_limit_  = 8f;
        // warp_position_ = transform.position.y;
    }

    private void InitializeScale()
    {
        /*float offset_x =
    (0.12f * transform.localScale.x - 0.12f) * 0.5f;

        float offset_y =
            (0.12f * transform.localScale.y - 0.12f) * 0.5f;

        transform.GetChild((int)Child.LEFT).position =
            new Vector3(
                transform.GetChild((int)Child.LEFT).position.x - offset_x,
                transform.GetChild((int)Child.LEFT).position.y,
                transform.GetChild((int)Child.LEFT).position.z);

        transform.GetChild((int)Child.RIGHT).position =
            new Vector3(
                transform.GetChild((int)Child.RIGHT).position.x + offset_x,
                transform.GetChild((int)Child.RIGHT).position.y,
                transform.GetChild((int)Child.RIGHT).position.z);

        transform.GetChild((int)Child.TOP).position =
            new Vector3(
                transform.GetChild((int)Child.TOP).position.x,
                transform.GetChild((int)Child.TOP).position.y + offset_y,
                transform.GetChild((int)Child.TOP).position.z);

        transform.GetChild((int)Child.BOTTOM).position =
            new Vector3(
                transform.GetChild((int)Child.BOTTOM).position.x,
                transform.GetChild((int)Child.BOTTOM).position.y - offset_y,
                transform.GetChild((int)Child.BOTTOM).position.z);

        offset_x =
            (0.18f * transform.localScale.x - 0.18f) * 0.5f;

        offset_y =
            (0.18f * transform.localScale.y - 0.18f) * 0.5f;

        transform.GetChild((int)Child.TOP_LEFT).position =
            new Vector3(
                transform.GetChild((int)Child.TOP_LEFT).position.x - offset_x,
                transform.GetChild((int)Child.TOP_LEFT).position.y + offset_y,
                transform.GetChild((int)Child.TOP_LEFT).position.z);

        transform.GetChild((int)Child.TOP_RIGHT).position =
            new Vector3(
                transform.GetChild((int)Child.TOP_RIGHT).position.x + offset_x,
                transform.GetChild((int)Child.TOP_RIGHT).position.y + offset_y,
                transform.GetChild((int)Child.TOP_RIGHT).position.z);

        transform.GetChild((int)Child.BOTTOM_RIGHT).position =
            new Vector3(
                transform.GetChild((int)Child.BOTTOM_RIGHT).position.x + offset_x,
                transform.GetChild((int)Child.BOTTOM_RIGHT).position.y - offset_y,
                transform.GetChild((int)Child.BOTTOM_RIGHT).position.z);

        transform.GetChild((int)Child.BOTTOM_LEFT).position =
            new Vector3(
                transform.GetChild((int)Child.BOTTOM_LEFT).position.x - offset_x,
                transform.GetChild((int)Child.BOTTOM_LEFT).position.y - offset_y,
                transform.GetChild((int)Child.BOTTOM_LEFT).position.z);

        transform.GetChild((int)Child.TOP_LEFT).localScale =
            new Vector3(
                transform.GetChild((int)Child.TOP_LEFT).localScale.x / transform.localScale.y,
                transform.GetChild((int)Child.TOP_LEFT).localScale.y / transform.localScale.x,
                transform.GetChild((int)Child.TOP_LEFT).localScale.z);

        transform.GetChild((int)Child.TOP_RIGHT).localScale =
            new Vector3(
                transform.GetChild((int)Child.TOP_RIGHT).localScale.x / transform.localScale.x,
                transform.GetChild((int)Child.TOP_RIGHT).localScale.y / transform.localScale.y,
                transform.GetChild((int)Child.TOP_RIGHT).localScale.z);

        transform.GetChild((int)Child.BOTTOM_RIGHT).localScale =
            new Vector3(
                transform.GetChild((int)Child.BOTTOM_RIGHT).localScale.x / transform.localScale.y,
                transform.GetChild((int)Child.BOTTOM_RIGHT).localScale.y / transform.localScale.x,
                transform.GetChild((int)Child.BOTTOM_RIGHT).localScale.z);

        transform.GetChild((int)Child.BOTTOM_LEFT).localScale =
            new Vector3(
                transform.GetChild((int)Child.BOTTOM_LEFT).localScale.x / transform.localScale.x,
                transform.GetChild((int)Child.BOTTOM_LEFT).localScale.y / transform.localScale.y,
                transform.GetChild((int)Child.BOTTOM_LEFT).localScale.z);

        offset_x =
            ((0.18f * (transform.localScale.x - 1.0f)) * 2.0f + 0.96f * transform.localScale.x) / (0.32f * transform.localScale.x);

        offset_y =
            ((0.18f * (transform.localScale.y - 1.0f)) * 2.0f + 0.96f * transform.localScale.y) / (0.32f * transform.localScale.y);

        transform.GetChild((int)Child.LEFT).localScale =
            new Vector3(
                offset_y,
                transform.GetChild((int)Child.LEFT).localScale.y / transform.localScale.x,
                transform.GetChild((int)Child.LEFT).localScale.z);

        transform.GetChild((int)Child.TOP).localScale =
            new Vector3(
                offset_x,
                transform.GetChild((int)Child.TOP).localScale.y / transform.localScale.y,
                transform.GetChild((int)Child.TOP).localScale.z);

        transform.GetChild((int)Child.RIGHT).localScale =
            new Vector3(
                offset_y,
                transform.GetChild((int)Child.RIGHT).localScale.y / transform.localScale.x,
                transform.GetChild((int)Child.RIGHT).localScale.z);

        transform.GetChild((int)Child.BOTTOM).localScale =
            new Vector3(
                offset_x,
                transform.GetChild((int)Child.BOTTOM).localScale.y / transform.localScale.y,
                transform.GetChild((int)Child.BOTTOM).localScale.z);*/
    }

    /// <summary>
    /// １フレーム毎の更新処理をします
    /// </summary>
    private void Update()
    {
        if (!is_pause_)
        {
            if (transform.position.y >= scroll_limit_)
            {
                transform.position = new Vector3(
                    transform.position.x,
                    warp_position_,
                    transform.position.z);
            }
            else
            {
                transform.position = new Vector3(
                    transform.position.x,
                    transform.position.y + scroll_speed_,
                    transform.position.z);
            }
        }
    }
}
