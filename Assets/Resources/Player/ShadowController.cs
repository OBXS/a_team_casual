﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShadowController : MonoBehaviour
{
    List<GameObject> playerOldGO;
    int time;

    public Material shadowMaterial;
    public GameObject shadowObject;
    public GameObject playerObject;
    public float shadowSpeed;
    public int delay; //frames

    public Color shadowColor;

    public bool jumpFlag;
    public bool wallFlag;

    // Use this for initialization
    void Start()
    {
        time = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (time == delay)
        {
            if (!playerObject.GetComponent<PlayerController>().isDead)
            {
                GameObject clone = Instantiate(shadowObject, playerObject.transform.position, playerObject.transform.rotation);

                //できない（泣く）
                //foreach (SpriteRenderer sprite in clone.transform.GetComponents<SpriteRenderer>())
                //{
                //    sprite.material.SetColor("_Color", new Color(0, 0, 0, 0));
                //}

                clone.transform.Find("PlayerPrefab").Find("PlayerFall").Find("Head").GetComponent<SpriteRenderer>().material.SetColor("_Color", shadowColor);
                clone.transform.Find("PlayerPrefab").Find("PlayerFall").Find("Torso").GetComponent<SpriteRenderer>().material.SetColor("_Color", shadowColor);
                clone.transform.Find("PlayerPrefab").Find("PlayerFall").Find("ShoulderLeft").GetComponent<SpriteRenderer>().material.SetColor("_Color", shadowColor);
                clone.transform.Find("PlayerPrefab").Find("PlayerFall").Find("ShoulderLeft").Find("ArmLeft").GetComponent<SpriteRenderer>().material.SetColor("_Color", shadowColor);
                clone.transform.Find("PlayerPrefab").Find("PlayerFall").Find("ShoulderRight").GetComponent<SpriteRenderer>().material.SetColor("_Color", shadowColor);
                clone.transform.Find("PlayerPrefab").Find("PlayerFall").Find("ShoulderRight").Find("ArmRight").GetComponent<SpriteRenderer>().material.SetColor("_Color", shadowColor);
                clone.transform.Find("PlayerPrefab").Find("PlayerFall").Find("ThighLeft").GetComponent<SpriteRenderer>().material.SetColor("_Color", shadowColor);
                clone.transform.Find("PlayerPrefab").Find("PlayerFall").Find("ThighLeft").Find("LegLeft").GetComponent<SpriteRenderer>().material.SetColor("_Color", shadowColor);
                clone.transform.Find("PlayerPrefab").Find("PlayerFall").Find("ThighRight").GetComponent<SpriteRenderer>().material.SetColor("_Color", shadowColor);
                clone.transform.Find("PlayerPrefab").Find("PlayerFall").Find("ThighRight").Find("LegRight").GetComponent<SpriteRenderer>().material.SetColor("_Color", shadowColor);

                clone.transform.Find("PlayerPrefab").Find("PlayerJump").Find("Head").GetComponent<SpriteRenderer>().material.SetColor("_Color", shadowColor);
                clone.transform.Find("PlayerPrefab").Find("PlayerJump").Find("Torso").GetComponent<SpriteRenderer>().material.SetColor("_Color", shadowColor);
                clone.transform.Find("PlayerPrefab").Find("PlayerJump").Find("ShoulderLeft").GetComponent<SpriteRenderer>().material.SetColor("_Color", shadowColor);
                clone.transform.Find("PlayerPrefab").Find("PlayerJump").Find("ShoulderLeft").Find("ArmLeft").GetComponent<SpriteRenderer>().material.SetColor("_Color", shadowColor);
                clone.transform.Find("PlayerPrefab").Find("PlayerJump").Find("ShoulderRight").GetComponent<SpriteRenderer>().material.SetColor("_Color", shadowColor);
                clone.transform.Find("PlayerPrefab").Find("PlayerJump").Find("ShoulderRight").Find("ArmRight").GetComponent<SpriteRenderer>().material.SetColor("_Color", shadowColor);
                clone.transform.Find("PlayerPrefab").Find("PlayerJump").Find("ThighLeft").GetComponent<SpriteRenderer>().material.SetColor("_Color", shadowColor);
                clone.transform.Find("PlayerPrefab").Find("PlayerJump").Find("ThighLeft").Find("LegLeft").GetComponent<SpriteRenderer>().material.SetColor("_Color", shadowColor);
                clone.transform.Find("PlayerPrefab").Find("PlayerJump").Find("ThighRight").GetComponent<SpriteRenderer>().material.SetColor("_Color", shadowColor);
                clone.transform.Find("PlayerPrefab").Find("PlayerJump").Find("ThighRight").Find("LegRight").GetComponent<SpriteRenderer>().material.SetColor("_Color", shadowColor);


                if (playerObject.GetComponent<PlayerController>().playerDir == PlayerController.Direction.Right)
                {
                    clone.transform.localScale = new Vector2(-clone.transform.localScale.x, clone.transform.localScale.y);
                }
                if (playerObject.transform.Find("PlayerPrefab").Find("PlayerFall").gameObject.activeSelf)
                {
                    clone.transform.Find("PlayerPrefab").Find("PlayerFall").gameObject.SetActive(true);
                }
                else if (playerObject.transform.Find("PlayerPrefab").Find("PlayerJump").gameObject.activeSelf)
                {
                    clone.transform.Find("PlayerPrefab").Find("PlayerJump").gameObject.SetActive(true);
                }

                clone.transform.Find("PlayerTargets").Find("ArmLTarget").transform.position = playerObject.transform.Find("PlayerTargets").Find("ArmLTarget").transform.position;
                clone.transform.Find("PlayerTargets").Find("ArmRTarget").transform.position = playerObject.transform.Find("PlayerTargets").Find("ArmRTarget").transform.position;
                clone.transform.Find("PlayerTargets").Find("LegLTarget").transform.position = playerObject.transform.Find("PlayerTargets").Find("LegLTarget").transform.position;
                clone.transform.Find("PlayerTargets").Find("LegRTarget").transform.position = playerObject.transform.Find("PlayerTargets").Find("LegRTarget").transform.position;
                clone.transform.Find("PlayerTargets").Find("ThighLTarget").transform.position = playerObject.transform.Find("PlayerTargets").Find("ThighLTarget").transform.position;
                clone.transform.Find("PlayerTargets").Find("ThighRTarget").transform.position = playerObject.transform.Find("PlayerTargets").Find("ThighRTarget").transform.position;
                clone.transform.Find("PlayerTargets").Find("ElbowLTarget").transform.position = playerObject.transform.Find("PlayerTargets").Find("ElbowLTarget").transform.position;
                clone.transform.Find("PlayerTargets").Find("ElbowRTarget").transform.position = playerObject.transform.Find("PlayerTargets").Find("ElbowRTarget").transform.position;
                clone.transform.Find("PlayerTargets").Find("HeadTarget").transform.position = playerObject.transform.Find("PlayerTargets").Find("HeadTarget").transform.position;

                clone.GetComponent<Rigidbody2D>().velocity = new Vector2(0.0f, shadowSpeed);
                clone.transform.SetParent(transform);
            }
            else
            {
                Destroy(gameObject);
            }


            time = 0;
        }
        time++;
    }
}
