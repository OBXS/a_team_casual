﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public enum Direction
    {
        Min,
        Left,
        Up,
        Right,
        Down,

        Max
    }

    public float fallingSpeedMultiplier;    //重力の乗数
    public int doubleJump;                  //X段ジャンプの設定
    public float collisionDistance;         //プレイヤーの真ん中からX単位先に当たり判定を判断する
    public bool useExponential;             //加速するかどうかのフラグ
    public float jumpingPower;              //ジャンプする力の強さ
    public int allowPlayerJump = 0;         //残りX回空中ジャンプできる
    public float wallPos;

    //flags
    public bool jumpFlag;
    public bool wallFlag;

    Vector3 firstPos;                       //最初的に触ったところ
    Vector3 lastPos;                        //最終的に触ったところ
    float dragDistance;                     //この距離を超えたら、フリックとして処理する

    //bool playerUpdateAvailable = false;
    float gravity = 9.8f;                   //重力の強さ
    Direction gravityDir = Direction.Min;   //重力の方向

    public bool isDead;

    Rigidbody2D rb2d;
    bool doAction = false;
    public Direction playerDir;

    // Use this for initialization
    void Start()
    {
        dragDistance = Screen.width * 5 / 100; //マジックナンバー・・Publicにしたほうがいいかな？
        rb2d = GetComponent<Rigidbody2D>();
        allowPlayerJump = 0;
        SetGravity(Direction.Left);
        if (!useExponential)
        {
            rb2d.gravityScale = 0.0f;
        }
        playerDir = Direction.Left;
        GameObject shadow = Resources.Load<GameObject>("Player/Objects/PlayerShadowController");
        shadow.GetComponent<ShadowController>().playerObject = gameObject;
        Instantiate(shadow);
    }

    // Update is called once per frame
    void Update()
    {
        jumpFlag = true;
        wallFlag = false;
        //ani.SetBool("Jump", true);
        //ani.SetBool("Touched Wall", false);

        if (rb2d.velocity.x == 0)
        {
            jumpFlag = false;
            wallFlag = true;
            //ani.SetBool("Jump", false);
        }

        if (playerDir == Direction.Left)
        {
            transform.localScale = new Vector3(-0.65f, 0.65f, 1);
            transform.localEulerAngles = new Vector3(0, 0, -30);
        }
        else if (playerDir == Direction.Right)
        {
            transform.localScale = new Vector3(0.65f, 0.65f, 1);
            transform.localEulerAngles = new Vector3(0, 0, 30);
        }

        //float dist = RaycastCollision(Direction.Left);
        if (transform.position.x < (-wallPos + collisionDistance))
        {
            //コリジョンに入ったら、重力によって跳ねり返す
            transform.position = new Vector3(-wallPos + collisionDistance, transform.position.y);
            rb2d.velocity = new Vector2(0.0f, 0.0f);
            if (gravityDir == Direction.Left)
            {
                //プレイヤーを止める
                rb2d.gravityScale = 0.0f;

                //ジャンプを復旧する
                allowPlayerJump = doubleJump;
                wallFlag = true;
                //ani.SetBool("Touched Wall", true);
                playerDir = Direction.Left;
            }
        }

        //dist = RaycastCollision(Direction.Right);
        if (transform.position.x > (wallPos - collisionDistance))
        {
            //コリジョンに入ったら、重力によって跳ねり返す
            transform.position = new Vector3(wallPos - collisionDistance, transform.position.y);
            rb2d.velocity = new Vector2(0.0f, 0.0f);
            if (gravityDir == Direction.Right)
            {
                //プレイヤーを止める
                rb2d.gravityScale = 0.0f;
                //ジャンプを復旧する
                allowPlayerJump = doubleJump;
                wallFlag = true;
                //ani.SetBool("Touched Wall", true);
                playerDir = Direction.Right;
            }
        }

        float blockDist = RaycastCollision(Direction.Down);
        if (blockDist < collisionDistance)
        {
            isDead = true;
        }
        blockDist = RaycastCollision(Direction.Left);
        if (blockDist < collisionDistance)
        {
            isDead = true;
        }
        blockDist = RaycastCollision(Direction.Right);
        if (blockDist < collisionDistance)
        {
            isDead = true;
        }

        ParabolicJump();

        if (allowPlayerJump != 0)
        {
            if (Input.touchCount == 1)
            {
                Touch touch = Input.GetTouch(0);        //インプットを処理する
                if (touch.phase == TouchPhase.Began)    //タッチの始まり
                {
                    firstPos = touch.position;
                    lastPos = touch.position;
                }
                else if (touch.phase == TouchPhase.Ended) //タッチの終わり
                {
                    lastPos = touch.position;
                    doAction = true;
                }
            }

            if (Input.GetKey(KeyCode.LeftArrow))
            {
                if (gravityDir != Direction.Left)
                {
                    SetGravity(Direction.Left);
                    allowPlayerJump--;
                    
                }
            }
            else if (Input.GetKey(KeyCode.RightArrow))
            {
                if (gravityDir != Direction.Right)
                {
                    SetGravity(Direction.Right);
                    allowPlayerJump--;
                }
            }
            else if (Input.GetKeyDown(KeyCode.Space))
            {
                if (gravityDir == Direction.Right)
                {
                    SetGravity(Direction.Left);
                    playerDir = Direction.Right;
                    allowPlayerJump--;
                    rb2d.velocity = new Vector2(rb2d.velocity.x * 0.25f, rb2d.velocity.y);
                    if(!jumpFlag)
                    {
                        //rb2d.velocity = new Vector2(rb2d.velocity.x * 0.25f, 5);
                    }
                }
                else
                {
                    SetGravity(Direction.Right);
                    playerDir = Direction.Left;
                    allowPlayerJump--;
                    rb2d.velocity = new Vector2(rb2d.velocity.x * 0.25f, rb2d.velocity.y);
                    if (!jumpFlag)
                    {
                        //rb2d.velocity = new Vector2(rb2d.velocity.x * 0.25f, 5);
                    }
                }
            }
        }
        if (doAction)
        {
            //フリックですか？
            if (Mathf.Abs(lastPos.x - firstPos.x) > dragDistance || Mathf.Abs(lastPos.y - firstPos.y) > dragDistance)
            {
                //縦フリックですか？横フリックですか？
                if (Mathf.Abs(lastPos.x - firstPos.x) > Mathf.Abs(lastPos.y - firstPos.y))
                {
                    //横でした！
                    //左ですか？右ですか？
                    if ((lastPos.x > firstPos.x))
                    {
                        if (gravityDir != Direction.Right)
                        {
                            SetGravity(Direction.Right);
                            allowPlayerJump--;
                        }
                    }
                    else
                    {
                        if (gravityDir != Direction.Left)
                        {
                            SetGravity(Direction.Left);
                            allowPlayerJump--;
                        }
                    }
                }
                else
                {
                    //縦でした！
                }
            }
            else
            {
                //ただのタップ
                switch (gravityDir)
                {
                    case Direction.Left:
                        {
                            rb2d.velocity = new Vector2(jumpingPower, rb2d.velocity.y);
                        }
                        break;
                    case Direction.Right:
                        {
                            rb2d.velocity = new Vector2(-jumpingPower, rb2d.velocity.y);
                        }
                        break;
                    default: break;
                }
                rb2d.gravityScale = 1.0f;
                allowPlayerJump--;
            }
            doAction = false;
        }
    }

    //重力によってRayを作る、戻り値はRayの長さ
    float RaycastCollision(Direction rayDir)
    {
        RaycastHit hit;

        switch (rayDir)
        {
            case Direction.Left:
                {
                    if (Physics.Raycast(transform.position, transform.TransformDirection(Vector2.left), out hit, Mathf.Infinity))
                    {
                        Debug.DrawRay(transform.position, hit.point, Color.red);
                        return hit.distance;
                    }
                }
                break;
            case Direction.Right:
                {
                    if (Physics.Raycast(transform.position, transform.TransformDirection(Vector2.right), out hit, Mathf.Infinity))
                    {
                        Debug.DrawRay(transform.position, hit.point, Color.red);
                        return hit.distance;
                    }
                }
                break;
            case Direction.Down:
                {
                    if (Physics.Raycast(transform.position, transform.TransformDirection(Vector2.down), out hit, Mathf.Infinity))
                    {
                        Debug.DrawRay(transform.position, hit.point, Color.blue);
                        return hit.distance;
                    }
                }
                break;
            default: break;
        }
        return 100;
    }

    //重力の設定関数、加速の設定もここに入っています
    bool SetGravity(Direction direction)
    {
        if (gravityDir != direction)
        {
            if (useExponential)
            {
                switch (direction)
                {
                    case Direction.Left:
                        {
                            Physics2D.gravity = new Vector2(-gravity * fallingSpeedMultiplier, 0.0f);
                            gravityDir = Direction.Left;
                        }
                        break;
                    case Direction.Right:
                        {
                            Physics2D.gravity = new Vector2(gravity * fallingSpeedMultiplier, 0.0f);
                            gravityDir = Direction.Right;
                        }
                        break;
                    default: break;
                }
                GetComponent<Rigidbody2D>().gravityScale = 1.0f;
            }
            else
            {
                switch (direction)
                {
                    case Direction.Left:
                        {
                            rb2d.velocity = new Vector2(-gravity * fallingSpeedMultiplier * 0.25f, rb2d.velocity.y);
                            gravityDir = Direction.Left;
                        }
                        break;
                    case Direction.Right:
                        {
                            rb2d.velocity = new Vector2(gravity * fallingSpeedMultiplier * 0.25f, rb2d.velocity.y);
                            gravityDir = Direction.Right;
                        }
                        break;
                    default: break;
                }
            }
            return true;
        }
        return false;
    }

    public bool IsJumping()
    {
        if (rb2d.velocity.x == 0)
        {
            return false;
        }
        return true;
    }

    public void SetFallSpeed(float speed)
    {
        fallingSpeedMultiplier = speed;
    }

    public void SetJumpSpeed(float speed)
    {
        jumpingPower = speed;
    }

    void ParabolicJump()
    {
        if (!jumpFlag)
        {
            if (transform.position.y == 2.5f)
            {
                rb2d.velocity = new Vector2(rb2d.velocity.x, 0);
                transform.position += new Vector3(0, 0.1f, 0);
            }
            else if (transform.position.y < 2.5f)
            {
                if (rb2d.velocity.y < 2)
                {
                    rb2d.velocity += new Vector2(0, 0.8f);
                }
            }
            else
            {
                if (rb2d.velocity.y > -2)
                {
                    rb2d.velocity -= new Vector2(0, 0.8f);
                }
            }
        }
        else
        {
            rb2d.velocity = new Vector2(rb2d.velocity.x, 0);
        }
    }
}
