﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyObject : MonoBehaviour
{
    public float destroyTreshold;
    //void OnBecameInvisible()
    //{
    //    Debug.Log("in");
    //    Destroy(gameObject);
    //}

    // Use this for initialization
    void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (transform.position.y > destroyTreshold)
        {
            Destroy(gameObject);
        }
	}
}
