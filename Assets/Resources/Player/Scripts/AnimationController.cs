﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationController : MonoBehaviour
{
    Animator ani;
    public PlayerController pc;
    public bool isShadow;

    bool jump;
    bool wall;
    // Use this for initialization
    void Start()
    {
        ani = GetComponent<Animator>();
        pc = GameObject.Find("PlayerComplete").GetComponent<PlayerController>();
        transform.Find("PlayerPrefab").Find("PlayerDead").gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        jump = pc.jumpFlag;
        wall = pc.wallFlag;

        if (wall)
        {
            transform.Find("PlayerPrefab").Find("PlayerFall").gameObject.SetActive(true);
            transform.Find("PlayerPrefab").Find("PlayerJump").gameObject.SetActive(false);
        }

        else if (jump)
        {
            transform.Find("PlayerPrefab").Find("PlayerFall").gameObject.SetActive(false);
            transform.Find("PlayerPrefab").Find("PlayerJump").gameObject.SetActive(true);
        }
        
        if (pc.isDead)
        {
            transform.Find("PlayerPrefab").Find("PlayerFall").gameObject.SetActive(false);
            transform.Find("PlayerPrefab").Find("PlayerJump").gameObject.SetActive(false);
            transform.Find("PlayerPrefab").Find("PlayerDead").gameObject.SetActive(true);
        }

        ani.SetBool("Jump", jump);
        ani.SetBool("Touched Wall", wall);

        if (isShadow)
        {
            this.enabled = false;
        }
    }
}
