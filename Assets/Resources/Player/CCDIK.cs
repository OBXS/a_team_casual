﻿using UnityEngine;
using System.Collections.Generic;

[ExecuteInEditMode]
public class CCDIK : MonoBehaviour
{
    public int iterations = 5;

    [Range(0.01f, 1)]
    public float damping = 1;

    public Transform target;
    public Transform endTransform;

    public Node[] angleLimits = new Node[0];

    Dictionary<Transform, Node> nodeCache;
    [System.Serializable]
    public class Node
    {
        public Transform transform;
        public float min;
        public float max;
    }

    void OnValidate()
    {
        foreach (Node node in angleLimits)
        {
            node.min = Mathf.Clamp(node.min, 0.0f, 360.0f);
            node.max = Mathf.Clamp(node.max, 0.0f, 360.0f);
        }
    }

    void Awake()
    {
        nodeCache = new Dictionary<Transform, Node>(angleLimits.Length);
        foreach (Node node in angleLimits)
        {
            if (!nodeCache.ContainsKey(node.transform))
            {
                nodeCache.Add(node.transform, node);
            }
        }
    }

    void LateUpdate()
    {
        if (!Application.isPlaying)
        {
            Awake();
        }

        if (target == null || endTransform == null) return;

        for (int i = 0; i < iterations; i++)
        {
            calucateIK();
        }

        endTransform.rotation = target.rotation;
    }

    void calucateIK()
    {
        Transform node = endTransform.parent;

        while (true)
        {
            rotateTowardsTarget(node);

            if (node == transform) break;

            node = node.parent;
        }
    }

    void rotateTowardsTarget(Transform transform)
    {
        Vector2 toTarget = target.position - transform.position;
        Vector2 toEnd = endTransform.position - transform.position;

        float angle = signedAngle(toEnd, toTarget);

        angle *= damping;

        angle = -(angle - transform.eulerAngles.z);

        if (nodeCache.ContainsKey(transform))
        {
            Node node = nodeCache[transform];
            float parentRotation = transform.parent ? transform.parent.eulerAngles.z : 0.0f;
            angle -= parentRotation;
            angle = clampAangle(angle, node.min, node.max);
            angle += parentRotation;
        }

        transform.rotation = Quaternion.Euler(0.0f, 0.0f, angle);
    }

    public static float signedAngle(Vector3 a, Vector3 b)
    {
        float angle = Vector3.Angle(a, b);
        float sign = Mathf.Sign(Vector3.Dot(Vector3.back, Vector3.Cross(a, b)));

        return angle * sign;
    }

    float clampAangle(float angle, float min, float max)
    {
        angle = Mathf.Abs((angle % 360) + 360) % 360;
        return Mathf.Clamp(angle, min, max);
    }
}